#!/bin/bash

devices=(IIUS)
root="/usr/sap/data/Striata"
RETVAL=0
# -1 : Required folders are not present
# -2 : All RDI files not matched, check for errors
# -3 : Files found in error directory

function checkFolders {
  if [ ! -d "$root"/tmp ] || [ ! -d "$root"/archive ] || [ ! -d "$root"/upload  ] || [ ! -d "$root"/error  ]; then
    echo "Folders missing, need /tmp, /archive, /upload and /error"
    exit -1
  fi
}

function processRDIs {
  for file in $( ls -p "$root"/*."$dev" 2>&1 | head -5 ); do
    #echo $file
    DOUTPUT_MEDIUM=0
    DEMC=0
    DIPDF=0
    BASENAME=$(basename $file)
        
    ggrep -A 6 "DOUTPUT_MEDIUM" $file | while read -r line; do
      #echo $line
      if [[ $line == "DOUTPUT_MEDIUM"* ]]; then
        DOUTPUT_MEDIUM=`echo $line | awk '{print $2}'`
      fi
      if [[ $line = "DEMC"* ]]; then
        DEMC=`echo $line | awk '{print $2}'`
        if [ $DEMC -eq 1 ] && [ ! -e $BASENAME.$DOUTPUT_MEDIUM.xls ]; then
          echo $BASENAME.$DOUTPUT_MEDIUM.xls " not found"
          mv "$BASENAME"* error/
        fi
      fi
      if [[ $line = "DIPDF"* ]]; then
        DIPDF=`echo $line | awk '{print $2}'`
        if [ $DIPDF -eq 1 ] && [ ! -e $BASENAME.$DOUTPUT_MEDIUM.xls ]; then
          echo $BASENAME.$DOUTPUT_MEDIUM.xls " not found"
          mv "$BASENAME"* error/
        fi
      fi
    done

    if [ -f $BASENAME ]; then
      mv "$BASENAME"* tmp/
    fi
  done
}

function loopThroughRDIFiles {
  for dev in ${devices[@]}; do
    FILES=$( ls -l "$root"/*."$dev" 2>&1 | grep -v "No such file or directory" | wc -l )
    echo "Files : " $FILES
 
    if [ "$FILES" -gt 0 ]; then
      processRDIs
    fi
  done
}

function checkMatched {
  MATCHED_FILES=$( ls -l "$root"/tmp/*."$dev" 2>&1 | grep -v "No such file or directory" | wc -l )
  echo "Matched RDI Files : " $MATCHED_FILES

  if [ "$MATCHED_FILES" -gt 0 ]; then
    # Get filename
    DATETIME=$(date +%Y%m%d-%H%M%S)
        
    # Zip files in /tmp/
    zip -j "$root"/upload/CHEP_"$dev"_$DATETIME.zip "$root"/tmp/*."$dev"*

    # Copy of zip file to archive
    cp "$root"/upload/CHEP_"$dev"_$DATETIME.zip "$root"/archive/

    # Change ownership of zip file so control-m can delete it
    chmod 664 "$root"/upload/CHEP_"$dev"_$DATETIME.zip
     
    # Remove all tmp files
    rm "$root"/tmp/*.$dev*
  #else
    #echo "All files not matched"
    #RETVAL=-2
  fi
}

function runAgain {
  for dev in ${devices[@]}; do
    FILES=$( ls -l "$root"/*."$dev" 2>&1 | grep -v "No such file or directory" | wc -l )
    if [ "$FILES" -gt 0 ] && [ $RETVAL -eq 0 ]; then
      sleep 10
      ./$0
    fi
  done
}

function cleanup {
  find "$root"/archive/ -mtime +30 -exec rm -f {} \;
}

function checkErrors {
  FILES=$( ls -l "$root"/error/* 2>&1 | grep -v "No such file or directory" | wc -l )
  if [ ! "$FILES" -eq 0 ]; then
    RETVAL=-3
  fi 
}

checkFolders
loopThroughRDIFiles
checkMatched
runAgain
cleanup
checkErrors

exit $RETVAL