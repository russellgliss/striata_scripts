#!/bin/bash

devices=(EUAI EUPI EUPA)
root="/usr/sap/data/Striata/"

# Check that the required folders exist
if [ -d "$root"tmp ] && [ -d "$root"archive ] && [ -d "$root"upload  ]; then
  for dev in ${devices[@]}; do
    FILES=$( ls -l "$root"*."$dev" 2>&1 | grep -v "No such file or directory" | wc -l )
    echo "Files : " $FILES
      
    if [ "$FILES" -gt 0 ]; then

      # Move files to /tmp/ limiting to 500
      for file in $( ls -p "$root"*."$dev" 2>&1 | head -500 ); do
        mv $file "$root"tmp/
        #mv "$root"*_"$dev" "$root"tmp/
      done
 
      # Get filename
      DATETIME=$(date +%Y%m%d-%H%M%S)
        
      # Zip files in /tmp/
      zip -j "$root"upload/CHEP_"$dev"_$DATETIME.zip "$root"tmp/*."$dev"

      # Copy of zip file to archive
      cp "$root"upload/CHEP_"$dev"_$DATETIME.zip "$root"archive/

      # Change ownership of zip file so control-m can delete it
      chmod 664 "$root"upload/CHEP_"$dev"_$DATETIME.zip
     
      # Remove all tmp files
      rm "$root"tmp/*.$dev
        
    fi
  done
else
  echo "Folders missing, need /tmp, /archive and /upload"
  exit -1
fi

# Check if there are more file, and if so, call itself again
for dev in ${devices[@]}; do
  FILES=$( ls -l "$root"*."$dev" 2>&1 | grep -v "No such file or directory" | wc -l )
  if [ "$FILES" -gt 0 ]; then
    sleep 10
    $0
  fi
done

# Remove files from archive, older than 30 days
find "$root"archive/ -mtime +30 -exec rm -f {} \;