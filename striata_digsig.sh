#!/bin/bash

root="/usr/sap/data/Striata/DigSig/"

# Check that the required folders exist
if [ -d "$root"tmp ] && [ -d "$root"archive ] && [ -d "$root"upload  ]; then

    FILES=$( ls -l "$root"*.PDF 2>&1 | grep -v "No such file or directory" | wc -l )
    echo "Files : " $FILES

    if [ "$FILES" -gt 0 ]; then

      # Move files to /tmp/ limiting to 500
      for file in $( ls -p "$root"*.PDF 2>&1 | head -500 ); do
        mv $file "$root"tmp/
      done

      # Get filename
      DATETIME=$(date +%Y%m%d-%H%M%S)
          PID=$$

      # Zip files in /tmp/
      zip -j "$root"upload/CHEP_EUDS_"$DATETIME"_"$PID".zip "$root"tmp/*.PDF

      # Copy of zip file to archive
      cp "$root"upload/CHEP_EUDS_"$DATETIME"_"$PID".zip "$root"archive/

      # Change ownership of zip file so control-m can delete it
      chmod 664 "$root"upload/CHEP_EUDS_"$DATETIME"_"$PID".zip

      # Remove all tmp files
      rm "$root"tmp/*$dev*.PDF

    fi

else
  echo "Folders missing, need /tmp, /archive and /upload"
  exit -1
fi

# Check if there are more file, and if so, call itself again

  FILES=$( ls -l "$root"*.PDF 2>&1 | grep -v "No such file or directory" | wc -l )
  if [ "$FILES" -gt 0 ]; then
    sleep 10
    $0
  fi

# Remove files from archive, older than 30 days
find "$root"archive/ -mtime +30 -exec rm -f {} \;
